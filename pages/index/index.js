//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    resultUrl: "http://img4.jiecaojingxuan.com/2016/B0E1B6B6-3E77-C6C4-6561-A9513DD1D368.gif",
    strDanmu: "",
    sendFriendDisable: true,
    creatingDamnu: false,
    creatingGif: false,
    phoneMode: 1 //0=ios  -1=android
  },
  bindKeyInput: function (e) {
    this.setData({
      strDanmu: e.detail.value
    })
  },
  //点击之后请求服务器，取得gif图片
  tapCreateGif: function (e) {
    console.log("create gif")
    if (this.data.strDanmu == '') {
      wx.showToast({
        title: '请先输入弹幕',
        icon: 'success',
        duration: 600
      })
      return;
    }
    var hello = this;
    hello.setData({
      creatingDamnu: true
    })
    wx.request({
      url: encodeURI('https://wxapp.jiecao.fm/img/str2gif?text=' + this.data.strDanmu),
      // data: {
      //   text: this.data.strDanmu
      // },
      method: 'POST',
      success: function (res) {
        console.log("create gif succ " + res.data)
        hello.setData({
          resultUrl: res.data,
          sendFriendDisable: false
        })
      },
      fail: function (res) {
        console.log(res)
        console.log('request faild')
      },
      complete: function () {
        hello.setData({
          creatingDamnu: false
        })
      }
    })
  },
  tapSendFriend: function () {
    // if (this.data.phoneMode == 0) {
    //   //如果是ios就再取得一次二维码
    //   var hello = this;
    //   hello.setData({
    //     creatingGif: true
    //   })
    //   wx.request({
    //     url: ('https://wxapp.test.jiecao.fm/img/str2d?url=' + this.data.resultUrl),
    //     data: {},
    //     method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    //     success: function (res) {
    //       // success
    //       console.log(res)
    //       wx.previewImage({
    //         urls: [res.data]
    //       })
    //     },
    //     fail: function () {
    //     },
    //     complete: function () {
    //       hello.setData({
    //         creatingGif: false
    //       })
    //     }
    //   })
    // } else {
      wx.previewImage({
        urls: [this.data.resultUrl]
      })
    // }
  },
  onLoad: function () {
    var hello = this;
    wx.getSystemInfo({
      success: function (res) {
        var mode = res.model.toLocaleLowerCase();
        hello.setData({//android
          phoneMode: mode.indexOf('iphone')
        })
        console.log('phone: ' + mode + ' mode ' + mode.indexOf('iphone'))
      }
    })
  }
})
